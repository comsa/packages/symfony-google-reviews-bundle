<?php

namespace Comsa\GoogleReviews\Repository;

use Comsa\GoogleReviews\Entity\LocationPeriod;
use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method LocationPeriod|null find($id, $lockMode = null, $lockVersion = null)
 * @method LocationPeriod|null findOneBy(array $criteria, array $orderBy = null)
 * @method LocationPeriod[] findAll()
 * @method LocationPeriod[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */

class LocationPeriodRepository extends EntityRepository
{

}
