<?php

namespace Comsa\GoogleReviews\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="comsa_google_location_periods")
 */
class LocationPeriod
{
  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $openDay;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $closedDay;

  /**
   * @ORM\Column(type="time")
   */
  private $openTime;

  /**
   * @ORM\Column(type="time")
   */
  private $closedTime;

  /**
   * @ORM\ManyToOne(targetEntity="Comsa\GoogleReviews\Entity\Location", inversedBy="periods")
   */
  private $location;

  public function getId(): int
  {
    return $this->id;
  }

  public function getOpenDay(): string
  {
    return $this->openDay;
  }

  public function setOpenDay(string $openDay): void
  {
    $this->openDay = $openDay;
  }

  public function getClosedDay(): string
  {
    return $this->closedDay;
  }

  public function getOpenTime(): \DateTime
  {
    return $this->openTime;
  }

  public function setOpenTime(\DateTime $dateTime): void
  {
    $this->openTime = $dateTime;
  }

  public function getClosedTime(): \DateTime
  {
    return $this->closedTime;
  }

  public function setClosedTime(\DateTime $dateTime): void
  {
    $this->closedTime = $dateTime;
  }
}
