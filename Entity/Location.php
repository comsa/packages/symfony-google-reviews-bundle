<?php

namespace Comsa\GoogleReviews\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="comsa_google_locations")
 *
 */
class Location
{
  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $locationId;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $name;

  /**
   * @ORM\ManyToOne(targetEntity="Comsa\GoogleReviews\Entity\Account", inversedBy="locations")
   */
  private $account;

  /**
   * @ORM\OneToMany(targetEntity="Comsa\GoogleReviews\Entity\Reviews", mappedBy="location", cascade={"persist", "remove"})
   * @ORM\JoinColumn(onDelete="CASCADE")
   */
  private $reviews;

  /**
   * @ORM\OneToMany(targetEntity="Comsa\GoogleReview\Entity\LocationPeriods", mappedBy="location", cascade={"persist", "remove"})
   * @ORM\JoinColumn(onDelete="CASCADE")
   */
  private $periods;

  public function __construct()
  {
    $this->reviews = new ArrayCollection();
    $this->periods = new ArrayCollection();
  }

  public function getId(): int
  {
    return $this->id;
  }

  public function getLocationId(): string
  {
    return $this->locationId;
  }

  public function setLocationId(string $locationId): void
  {
    $this->locationId = $locationId;
  }

  public function getName(): ?string
  {
    return $this->name;
  }

  public function setName(?string $name)
  {
    $this->name = $name;
  }

  public function getAccount(): Account
  {
    return $this->account;
  }

  public function setAccount(Account $account): void
  {
    $this->account = $account;
  }

  public function getReviews(): Collection
  {
    return $this->reviews;
  }

  public function addReview(Review $addedReview): void
  {
    $reviews = $this->reviews->filter(function (Review $review) use ($addedReview) {
      return $review->getReviewId() === $addedReview->getReviewId();
    });
  }

  public function removeReview(Review $review): void
  {
    if ($this->reviews->contains($review))
    {
      $this->reviews->removeElement($review);
    }
  }

  public function getPeriods(): Collection
  {
    return $this->periods;
  }

  public function addPeriod(LocationPeriod $addedLocationPeriod): void
  {
    $locationPeriods = $this->periods->filter(function (Location $location) use ($addedLocationPeriod) {
      return $location->getLocationPeriodId() === $addedLocationPeriod->getLocationPeriodId();
    });
  }

  public function removePeriod(LocationPeriod $locationPeriod): void
  {
    if ($this->periods->contains($locationPeriod))
    {
      $this->periods->removeElement($locationPeriod);
    }
  }
}
