<?php

namespace Comsa\GoogleReviews\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use function foo\func;

/**
 * @ORM\Entity
 * @ORM\Table(name="comsa_google_accounts")
 */
class Account
{
  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $accountId;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $name;

  /**
   * @ORM\OneToMany(targetEntity="Comsa\GoogleReviews\Entity\Location", mappedBy="account", cascade={"persist", "remove"})
   * @ORM\JoinColumn(onDelete="CASCADE")
   */
  private $locations;

  public function __construct()
  {
    $this->locations = new ArrayCollection();
  }

  public function getId()
  {
    return $this->id;
  }

  public function getAccountId(): string
  {
    return $this->accountId;
  }

  public function setAccountId(string $accountId): void
  {
    $this->accountId = $accountId;
  }

  public function getLocations(): Collection
  {
    return $this->locations;
  }

  public function addLocation(Location $addedLocation)
  {
    $locations = $this->locations->filter(function (Location $location) use ($addedLocation) {
      return $location->getLocationId() === $addedLocation->getLocationId();
    });

    if ($locations->isEmpty())
    {
      $this->locations->add($location);
    }
  }

  public function removeLocation(Location $location)
  {
    if ($this->locations->contains($location)) {
      $this->locations->removeElement($location);
    }
  }
}
