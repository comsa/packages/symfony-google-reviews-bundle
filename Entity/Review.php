<?php

namespace Comsa\GoogleReviews\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="comsa_google_reviews")
 */
class Review
{
  /**
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $reviewId;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $startRating;

  /**
   * @ORM\Column(type="text")
   */
  private $comment;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $reviewer;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $profilePhoto;

  /**
   * @ORM\Column(type="datetime")
   */
  private $createdOn;

  /**
   * @ORM\Column(type="datetime")
   */
  private $updatedAt;

  /**
   * @ORM\Column(type="string", length=2)
   */
  private $locale;

  /**
   * @ORM\ManyToOne(targetEntity="Comsa\GoogleReviews\Entity\Location", inversedBy="reviews")
   */
  private $location;

  public function getId(): int
  {
    return $this->id;
  }

  public function getReviewId(): string
  {
    return $this->reviewId;
  }

  public function setReviewId(string $reviewId): void
  {
    $this->reviewId = $reviewId;
  }

  public function getStarRating(): string
  {
    return $this->startRating;
  }

  public function setStarRating(string $starRating): void
  {
    $this->startRating = $starRating;
  }

  public function getComment(): string
  {
    return $this->comment;
  }

  public function setComment(string $comment): void
  {
    $this->comment = $comment;
  }

  public function getReviewer(): string
  {
    return $this->reviewer;
  }

  public function setReviewer(string $reviewer): void
  {
    $this->reviewer = $reviewer;
  }

  public function getProfilePhoto(): string
  {
    return $this->profilePhoto;
  }

  public function getCreatedOn(): \DateTime
  {
    return $this->createdOn;
  }

  public function setCreatedOn(\DateTime $createdOn): void
  {
    $this->createdOn = $createdOn;
  }

  public function getUpdatedAt(): \DateTime
  {
    return $this->updatedAt;
  }

  public function setUpdatedAt(\DateTime $updatedAt): void
  {
    $this->updatedAt = $updatedAt;
  }

  public function getLocale(): string
  {
    return $this->locale;
  }

  public function setLocale(string $locale)
  {
    $this->locale = $locale;
  }

  public function getLocation(): Location
  {
    return $this->location;
  }

  public function setLocation(Location $location): void
  {
    $this->location = $location;
  }
}
